const winston = require('winston')
const pkg = require(`${process.cwd()}/package.json`)

const levels = {
  error: 0,
  warn: 1,
  info: 2,
  debug: 3
}
const colors = {
  debug: 'blue',
  info: 'green',
  warn: 'yellow',
  error: 'red'
}

winston.addColors(colors)
const logger = winston.createLogger({
  level: 'info',
  format: winston.format.json(),
  defaultMeta: {
    app: pkg.name,
    version: pkg.version
  },
  transports: [
    new winston.transports.Console({
      format: winston.format.combine(
        winston.format.colorize(),
        winston.format.simple()
      ),
      colorize: true,
    }),
  ],
  levels
})

module.exports = {
  logger
}
