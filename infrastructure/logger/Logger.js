const NotImplementedError = require('../../utils/NotImplementedError')

class Logger {
  debug(message, objectToLog = {}) {
    throw new NotImplementedError()
  }
  info(message, objectToLog = {}) {
    throw new NotImplementedError()
  }
  warn(message, objectToLog = {}) {
    throw new NotImplementedError()
  }
  error(message, objectToLog = {}) {
    throw new NotImplementedError()
  }
}

module.exports = Logger