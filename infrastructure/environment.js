const assert = require('assert')
const path = require('path')
const { logger } = require('./logger')

const environmentVariablesToAssert = () => {
  assertVariable(process.env.NODE_ENV, 'NODE_ENV')
  assertVariable(process.env.RESTAPI_PORT, 'RESTAPI_PORT')
  assertVariable(process.env.RESTAPI_VERSION, 'RESTAPI_VERSION')
}

const checkEnvVars = () => {
  try {
    environmentVariablesToAssert()
  } catch (error) {
    logger.error(error.message)
    throw new Error(error.message)
  }
}

const loadEnvVars = () => {
  const location = path.join(__dirname, '/../.env')
  require('dotenv').config({ path: location })

  checkEnvVars()
}

const assertVariable = (variable, name) => {
  if (variable === undefined) {
    throw new Error(`Variable ${name} is undefined!`)
  }
  if (variable === "") {
    throw new Error(`Variable ${name} is empty!`)
  }
}

module.exports = {
  environmentVariablesToAssert,
  assertVariable,
  loadEnvVars
}
