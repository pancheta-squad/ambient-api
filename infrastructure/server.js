require('../infrastructure/environment').loadEnvVars()
const { logger } = require('./logger')
const app = require('../index')
const http = require('http')

const onError = (error) => {
  logger.error(error.message, { stack: error.stack })
}

const onListening = () => {
  const addr = server.address()
  logger.info(`Server is listening on port ${addr.port}`)
}

app.set('port', process.env.RESTAPI_PORT)

const server = http.createServer(app)
server.listen(process.env.RESTAPI_PORT)
server.on('error', onError)
server.on('listening', onListening)
