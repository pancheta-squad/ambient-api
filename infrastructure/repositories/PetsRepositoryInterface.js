const NotImplementedError = require('../../utils/NotImplementedError')

class PetsRepositoryInterface {
  static async create (pet) {
    throw new NotImplementedError()
  }

  static async findAll () {
    throw new NotImplementedError()
  }

  static async findById (id) {
    throw new NotImplementedError()
  }
}

module.exports = PetsRepositoryInterface