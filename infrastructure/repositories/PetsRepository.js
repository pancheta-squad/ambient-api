const PetsRepositoryInterface = require('./PetsRepositoryInterface')
const { getDB, ObjectId } = require('../mongoConnection')

class PetsRepository extends PetsRepositoryInterface {
  static async create (pet) {
    const db = await getDB()
    const { insertedId } = await db.collection('Pets')
    .insertOne(
      {
        ...pet,
        deletedAt: '',
        updatedAt: new Date().toISOString(),
        createdAt: new Date().toISOString()
      })
      return insertedId
    }
    
    static async findOneForHealthz () {
      try {
        const { getDB } = require('../mongoConnection')
        await getDB()
        return 'ok'
      } catch (error) {
        const { logger } = require('../Adapters')

        logger.error('Error in health database access', {
          message: error.message,
          stack: error.stack
        })
        return 'ko'
      }
  }

  static async findAll () {
    const db = await getDB()
    return await db.collection('Pets')
      .find().toArray()
  }

  static async find (query) {
    const db = await getDB()
    return await db.collection('Pets')
      .find(query).toArray()
  }

  static async findById (id) {
    const db = await getDB()
    return db.collection('Pets')
      .findOne({
        '_id': ObjectId(id)
      })
  }
}

module.exports = PetsRepository
