const Errors = require('./Errors')
const generateV4UUID = require("../../utils/generateUUID")

class ErrorAdapter extends Errors {
  constructor(error, serviceStatusCode = 500) {
    super()
    this.errorId = generateV4UUID()
    this.message = error.message
    this.code = error.statusCode || serviceStatusCode
    this.isOwn = error.isOwn || false
    this.data = error.data || {}
    this.type = Object.getPrototypeOf(error).constructor.name
    this.status = 'ko'
    this.stack = error.stack
    this.createdAt = new Date().toISOString()
  }

  toLog() {
    return {
      errorId: this.errorId,
      message: this.message,
      code: this.code,
      isOwn: this.isOwn,
      data: this.data,
      type: this.type,
      status: 'ko',
      stack: this.stack,
      createdAt: this.createdAt
    }
  }

  toResponse () {
    return {
      id: this.errorId,
      message: this.message,
      type: this.type,
      status: 'ko',
      createdAt: this.createdAt
    }
  }
}

module.exports = ErrorAdapter