const { PetsRepository } = require('./repositories')
const { logger } = require('./logger')

module.exports = {
	'petsRepository': PetsRepository,
	'logger': logger
}