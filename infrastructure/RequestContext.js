const asyncHooks = require('async_hooks')
const isObject = require('../utils/isObject')
const store = new Map();

const asyncHook = asyncHooks.createHook({
  init: (asyncId, _, triggerAsyncId) => {
    if (store.has(triggerAsyncId)) {
      store.set(asyncId, store.get(triggerAsyncId))
    }
  },
  destroy: (asyncId) => {
    if (store.has(asyncId)) {
      store.delete(asyncId)
    }
  }
});

asyncHook.enable();

const addToRequestContext = (key, data) => {
  const context = getRequestContext() || {}
  if (context[key] && isObject(context[key])) {
    context[key] = {...context[key], ...data}
  } else {
    context[key] = data
  }
  store.set(asyncHooks.executionAsyncId(), context)
  return context
};

const getRequestContext = () => {
  return store.get(asyncHooks.executionAsyncId())
};

const destroyRequestContext = () => {
  return asyncHook.destroy(asyncHooks.executionAsyncId())
};


module.exports = { addToRequestContext, getRequestContext, destroyRequestContext }