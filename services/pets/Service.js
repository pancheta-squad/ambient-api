const Pet = require('../../domain/pets/Pet')
const Criteria = require('../../domain/filtering/Criteria')
const NotAcceptableError = require('../../utils/NotAcceptableError')
const { addToRequestContext } = require('../../infrastructure/RequestContext')
const {
  petsRepository: Repository,
  logger
} = require('../../infrastructure/Adapters')

class Service {
  static async create (data) {
    logger.info('creating pet from request', data)
    
    const pet = Service.preparePetFrom(data)

    const document = pet.serialize()
    const insertedId = await Repository.create(document)
  
    addToRequestContext('controller', { createdPetId: insertedId })

    logger.info(`Pet ${insertedId} created`)
    return Boolean(insertedId) && document
  }

  static async findAll() {
    logger.info('Finding all pets request')

    return Repository.findAll()
  }

  static async findById(id) {
    logger.info(`Finding pet with id: ${id}`)

    return Repository.findById(id)
  }

  static async find(filters) {
    logger.info(`Finding pet with filters: ${filters}`)
    const filtering = new Criteria(filters)
    filtering.execute()
    
    return Repository.find(filtering.query)
  }

  static preparePetFrom(data) {
    const {
      name,
      bornAt,
      title,
      description,
      imagesURL
    } = data
    
    const pet = new Pet(name, bornAt)
    pet.addPetInfo(title, description, imagesURL)

    if (!pet.isValid()) {
      throw new NotAcceptableError(pet.errors.join('\n'))
    }

    return pet
  }
}

module.exports = Service
