# Pets Api

## Pets API

Pets API es una RestAPI que permite añadir mascotas por parte de usuarios y protectoras, ofreciendo una API pública para consumir.

## Requerimientos

- NodeJS: v14.17.0
- Mongo: v5.0
- npm
- Doppler (opcional)


## Arquitectura en uso

En el proyecto nos hemos decidido por usar una arquitectura hexagonal.

- Más información: [Arquitectura Hexagonal](./docs/ARQUITECTURA.md).

## Preparar la máquina del desarrollador 

### Actualización del archivo `README.md`

El archivo `README.md` se actualiza con cada *commit* con el 'tree' del proyecto y el resultado del 'coverage' de los test. Para ello tenemos preparado un hook del pre-commit que lo realiza automáticamente: 

- Debes descargar el archivo `https://gitlab.com/pancheta-squad/pets-api/-/snippets/2198032` y guardarlo en la carpeta `.git/hooks` con el nombre `pre-commit`.

- Debes dar permisos de ejecución al script `pre-commit`:

~~~
chmod +x pre-commit
~~~

### Instalar las dependencias

La aplicación está desarrollado con NodeJS, por lo que la primera vez se inicializó con `npm init` para crear la infraestructura mínima necesaria.

**Si estás clonando el proyecto**, debes instalarar las dependencias en tu máquina con `npm install`.

- Más información: [Npm](./docs/NPM.md).

### Datos de configuración de entorno

A continuación, en la carpeta `docs` tenemos un ejemplo del archivo `.env` que debe ir en la raíz y no se sube al repositorio (llamado `.env.example`) que puede utilizar el desarrollador para tener generar rápidamente el archivo en local. Recuerde que el archivo `.env` debe mantenerse fuera del repositorio (por lo que está incluido en el `.gitignore`).

El proyecto puede trabajar tanto con el archivo `.env` para aquellos colaboradores externos que lo necesiten, como a través de Doppler, el cual nos evita tener todas las variables y contraseñas en el archivo `.env`.

- Más información: [Doppler](./docs/DOPPLER.md).

### Uso de Docker

- Más información: [Docker](./docs/DOCKER.md).

### Script `petsapi.sh`

Para facilitar las tareas ordinarias del proyecto hemos preparado un script llamado `petsapi.sh`.

- Más información: [script petsapi.sh](./docs/SCRIPTPETSAPI.md).


## Lanzar los tests

### En un entorno sin Docker

Tenemos tres opciones para lanzar los tests:

1. Con los datos de configuración a través de el archivo `.env`.

~~~
docker run --network host -d mongo:5.0
npm test
~~~

2. Con los datos de configuración a través de Doppler.

~~~
docker run --network host -d mongo:5.0
doppler -c dev_env_local run -- npm test
~~~

3. Con el script `petsapi.sh`.

~~~
bash petsapi.sh test
~~~

### En un entorno con Docker

En el entorno dockerizado se lanzán con los datos de configuración a través de el archivo `.env`:

~~~
docker run --rm --network host --env-file .env pets-api npm test
~~~

O con los datos de configuración a través de Doppler:

~~~
docker run --rm --network host --env-file <(doppler -c dev_env_local secrets download --no-file --format docker) pets-api npm test
~~~

O con el script `petsapi.sh`:

~~~
bash petsapi.sh docker:test
~~~


## Levantar el API

### Con los datos a través de el archivo `.env`:

~~~
npm start
~~~

### Con los datos de configuración a través de Doppler:

~~~
doppler -c dev_env_local run -- npm start
~~~

O con el script `petsapi.sh`:

~~~
bash petsapi.sh start
~~~


## VPN Server

Pasos para levantar la aplicación en un VPN.

- Más información: [VPN](./docs/VPNServer.md).


## Bot de Telegram

Hemos activado un canal de Telegram para recibir las notificaciones de Gitlab-CI.

- Más información: [bot Telegram](./docs/TELEGRAM.md).


## FAQ

- Más información: [FAQ](./docs/FAQ.md).


## Tree

(El apartado 'Tree' se actualiza automáticamente con el hook 'pre-commit'.)

<div class='tree'><pre>
   |-README.md
   |-controllers
   |---pets
   |-----index.js
   |-domain
   |---pets
   |-----Pet.js
   |---filtering
   |-----ComparationOperators.js
   |-----implementations
   |-------LowerThan.js
   |-------EqualTo.js
   |-------GreaterThan.js
   |-----Criteria.js
   |---healthz
   |-----Healthz.js
   |-Dockerfile
   |-index.js
   |-petsapi.sh
   |-services
   |---pets
   |-----index.js
   |-----Service.js
   |---healthz
   |-----index.js
   |-----Service.js
   |-tests
   |---testSupport
   |-----pet.js
   |-----helpers.js
   |---unit
   |-----variables.test.js
   |---e2e
   |-----healthz.test.js
   |-----errors.test.js
   |-----pets.test.js
   |-----filtering.test.js
   |-middlewares
   |---index.js
   |-gitlab-ci
   |---sendMessageWithTelegram.sh
   |-Dockerfile.production
   |-package.json
   |-docs
   |---DOPPLER.md
   |---DOCKER.md
   |---ARQUITECTURA.md
   |---TELEGRAM.md
   |---NPM.md
   |---SCRIPTPETSAPI.md
   |---FAQ.md
   |---VPNServer.md
   |---images
   |-----firewall.png
   |-----Hexagonal.png
   |-utils
   |---NotImplementedError.js
   |---generateUUID.js
   |---NotAcceptableError.js
   |---isObject.js
   |-actions
   |---pets
   |-----index.js
   |-----Find.js
   |-----FindAll.js
   |-----Create.js
   |-----FindById.js
   |---healthz
   |-----index.js
   |-----HealthzAction.js
   |-routes
   |---pets.js
   |---healthz.js
   |-infrastructure
   |---repositories
   |-----PetsRepository.js
   |-----index.js
   |-----PetsRepositoryInterface.js
   |---Adapters.js
   |---server.js
   |---errors
   |-----ErrorAdapter.js
   |-----Errors.js
   |---environment.js
   |---mongoConnection.js
   |---logger
   |-----index.js
   |-----Logger.js
   |-----WinstonLogger.js
   |-----LoggerWinstonAdapter.js
   |---RequestContext.js
   |-package-lock.json
</pre></div>

## Test coverage 

(El apartado 'test coverage' se actualiza automáticamente con el hook 'pre-commit'.)

<table class="coverage-summary"></table>
