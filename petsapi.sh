#!{BASH}

redColor='\033[0;31m'
greenColor='\033[0;32m'
orangeColor='\033[0;33m'
purpleColor='\033[0;35m'
cyanColor='\033[0;36m'
resetColor='\033[0m'

case "${1}" in
  "start")
    doppler -c dev_env_local run -- npm start
  ;;
  "test")
    doppler -c dev_env_local run -- npm test
  ;;
  "prepare")
    npm install
    docker run --network host --name pets-mongo -d mongo:5.0
  ;;
  "docker:start")
    docker run --network host --name pets-mongo -d mongo:5.0
    result=$?
    if [ "${result}" != "0" ]
      then
        echo -e "\n${orangeColor}Probably 'pets-mongo' is already running.${resetColor}"
    fi
    DOPPLER_RESTAPI_PORT=$(doppler -c dev_env_local secrets get RESTAPI_PORT --plain)
    doppler -c dev_env_local run -- docker run --rm --network host --env-file <(doppler -c dev_env_local secrets download --no-file --format docker) -d --name pets-api pets-api npm start
    result=$?
    if [ "${result}" != "0" ]
      then
        echo -e "\n${orangeColor}Probably 'Pets-api' is already running.${resetColor}"
    fi
  ;;
  "docker:build")
    docker build -t pets-api .
  ;;
  "docker:stop")
    docker stop pets-api
    result=$?
    if [ "${result}" != "0" ]
      then
        echo -e "\n${redColor}We cannot stop 'pets-api'.${resetColor}"
    fi
    docker stop pets-mongo
    result=$?
    if [ "${result}" != "0" ]
      then
        echo -e "\n${redColor}We cannot stop 'pets-mongo'.${resetColor}"
    fi
  ;;
  "docker:test")
    docker build -t pets-api .
    docker run --network host --name pets-mongo -d mongo:5.0
    DOPPLER_RESTAPI_PORT=$(doppler -c dev_env_local secrets get RESTAPI_PORT --plain)
    doppler -c dev_env_local run -- docker run --rm --network host --env-file <(doppler -c dev_env_local secrets download --no-file --format docker) pets-api npm test
  ;;
  "docker:install")
    docker run --rm -d --name pets-api pets-api npm install
  ;;
  "docker:clean")
    docker stop $(docker ps -a -q) && docker rm $(docker ps -a -q)
  ;;
  *)
    echo -e "${cyanColor}Available commands:${resetColor}
  ${purpleColor}start${resetColor}   (launch 'npm start')
  ${purpleColor}test${resetColor}    (launch 'npm test')
  ${purpleColor}prepare${resetColor} (launch 'npm install' and up mongo docker container)
  ${purpleColor}docker:build${resetColor}   (build the 'pets-api' docker image)
  ${purpleColor}docker:start${resetColor}   (up the 'pets-api' docker image)
  ${purpleColor}docker:stop${resetColor}    (stop the 'pets-api' docker image)
  ${purpleColor}docker:test${resetColor}    (launch 'npm test' docker image)
  ${purpleColor}docker:install${resetColor} (launch 'npm install' docker image)"
  ;;
esac

