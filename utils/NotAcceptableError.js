class NotAcceptableError extends Error {
  constructor(message) {
    super(message)
    this.statusCode = 406
    this.isOwn = true
  }
}

module.exports = NotAcceptableError