require('./infrastructure/environment').loadEnvVars()

const express = require('express')
const app = express()
const Sentry = require('@sentry/node')
const middlewares = require('./middlewares')

const healthzRoutes = require('./routes/healthz')
const petsRoutes = require('./routes/pets')

const apiVersion = `/${process.env.RESTAPI_VERSION}`

Sentry.init({
  dsn: 'https://glet_329bdb3b75c6eaa8aef4607c4edfb6f0@gitlab.com/api/v4/error_tracking/collector/30051286'
})


app.use(express.json())
app.use(express.urlencoded({ extended: false }))

app.use(middlewares.cors)
app.use(middlewares.requestTracer)
app.use(middlewares.logInfo)
app.use(apiVersion, healthzRoutes)
app.use(apiVersion, petsRoutes)
app.use(middlewares.destroyContext)
app.use(Sentry.Handlers.errorHandler())
app.use(middlewares.error)
module.exports = app
