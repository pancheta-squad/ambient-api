const flush = async () => {
  const db = await require('./../../infrastructure/mongoConnection').getDB()
  await db.collection('Pets').deleteMany() 
}

const sleep = ms => {
  return new Promise((resolve, reject) => {
    let wait = setTimeout(() => {
      clearTimeout(wait)
      resolve()
    }, ms)
  })
}

module.exports = {
  flush,
  sleep
}
