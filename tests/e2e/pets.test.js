require('../../infrastructure/environment').loadEnvVars()
const NODE_ENV = 'testing'
process.env.NODE_ENV = NODE_ENV
const { flush } = require('../testSupport/helpers')
const request = require('supertest')
const app = require('../../index')
const {
  createPet,
  postAPet,
  preparePetAge
} = require('../testSupport/pet')
const SUCCESS = 200
const BAD_REQUEST = 400

describe('Pets Controller', () => {
  beforeEach(async () => {
    jest.resetAllMocks()
    await flush()
  })
  test('handles pets creation', async () => {
    const petAge = 4
    const date = preparePetAge(petAge)
    const pet = {
      name: 'Ahcul',
      bornAt: date,
      title: 'Ahcul is great!!!',
      description: 'Ahcul is a very funny dog, very kindle and confident',
      imagesURL: [
        'https://live.staticflickr.com/5088/5323961120_0172112bcb_b.jpg',
        'https://live.staticflickr.com/3433/3927529272_e6e5448807.jpg'
      ]
    }
    const response = await postAPet(pet)
    
    expect(response.statusCode).toBe(SUCCESS)
    expect(response.body.name).toBe(pet.name)
    expect(response.body.bornAt).toBe(date)
    expect(response.body.title).toBe(pet.title)
    expect(response.body.description).toBe(pet.description)
    expect(response.body.imagesURL).toMatchObject(pet.imagesURL)
  })
  test('create pets has required', async () => {
    const petAge = 4
    const date = preparePetAge(petAge)
    const pet = {
      name: '',
      bornAt: date,
      title: '',
      description: 'Ahcul is a very funny dog, very kindle and confident',
      imagesURL: 'https://live.staticflickr.com/5088/5323961120_0172112bcb_b.jpg'
    }

    const response = await postAPet(pet)
    
    expect(response.statusCode).toBe(BAD_REQUEST)
    expect(response.body.message).toBe('Field name is required.\nField title is required.\nField images needs to be an array.')
    expect(response.body.status).toBe('ko')
    expect(response.body.type).toBe('NotAcceptableError')
  })

  test('find all pets', async () => {
    const petAge = 4
    const date = preparePetAge(petAge)
    const ahculResponse = await createPet('Ahcul', date)
    const drakoResponse = await createPet('Drako', date)

    const response = await request(app)
    .get(`/${process.env.RESTAPI_VERSION}/pets`)

    expect(response.statusCode).toBe(SUCCESS)
    expect(response.body[0].name).toBe(ahculResponse.body.name)
    expect(response.body[0].bornAt).toBe(date)
    expect(response.body[0]._id).toBeTruthy()
    expect(response.body[1].name).toBe(drakoResponse.body.name)
  })

  test('find one pet by id', async () => {
    const petAge = 4
    const date = preparePetAge(petAge)
    const ahculResponse = await createPet('Ahcul', date)
    const petResponse = await request(app)
    .get(`/${process.env.RESTAPI_VERSION}/pets`)
    const url = `/${process.env.RESTAPI_VERSION}/pets/${petResponse.body[0]._id}`
    
    const response = await request(app)
    .get(url)
    
    expect(response.statusCode).toBe(SUCCESS)
    expect(response.body.name).toBe(ahculResponse.body.name)
    expect(response.body.bornAt.split(':')[0]).toBe(date.split(':')[0])
    expect(response.body._id).toBeTruthy()
  })
})