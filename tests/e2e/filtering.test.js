require('../../infrastructure/environment').loadEnvVars()
const NODE_ENV = 'testing'
process.env.NODE_ENV = NODE_ENV
const request = require('supertest')
const { flush, sleep } = require('../testSupport/helpers')
const app = require('../../index')
const {
  createPet,
  preparePetAge
} = require('../testSupport/pet')
const SUCCESS = 200

describe('Criteria Filtering', () => {
  beforeEach(async () => {
    jest.resetAllMocks()
    await flush()
    await sleep(200)
  })
  test('[gt] get pets with age greater than a date', async () => {
    const petAge = 4
    const date = preparePetAge(petAge)
    const lowerThanPetAgeDate = dateLowerThanPetAge(petAge)
    await createPet('Ahcul', date)
    const response = await request(app)
      .get(`/${process.env.RESTAPI_VERSION}/pets?filters=[bornAt][gt][${lowerThanPetAgeDate}]`)
    expect(response.statusCode).toBe(SUCCESS)
    expect(response.body[0].name).toBe('Ahcul')
  })
  test('[lt] get pets with age lower than a date', async () => {
    const petAge = 4
    const date = preparePetAge(petAge)
    const greaterThanPetAgeDate = dateGreaterThanPetAge(petAge)
    await createPet('Ahcul', date)
    const response = await request(app)
      .get(`/${process.env.RESTAPI_VERSION}/pets?filters=[bornAt][lt][${greaterThanPetAgeDate}]`)
    expect(response.statusCode).toBe(SUCCESS)
    expect(response.body[0].name).toBe('Ahcul')
  })
  test('[eq] get pets with a known age', async () => {
    const DumboAge = 3
    const DumboDate = preparePetAge(DumboAge)
    await createPet('Dumbo', DumboDate)
    const BambiAge = 5
    const BambiDate = preparePetAge(BambiAge)
    await createPet('Bambi', BambiDate)
    const AhculAge = 4
    const AhculDate = preparePetAge(AhculAge)
    const equalToAhculAgeDate = preparePetAge(AhculAge)
    await createPet('Ahcul', AhculDate)
    const response = await request(app)
      .get(`/${process.env.RESTAPI_VERSION}/pets?filters=[bornAt][eq][${equalToAhculAgeDate}]`)
    expect(response.statusCode).toBe(SUCCESS)
    expect(response.body.length).toBe(1)
    expect(response.body[0].name).toBe('Ahcul')
  })
})

const dateLowerThanPetAge = (age) => {
  return preparePetAge(age + 1)
}

const dateGreaterThanPetAge = (age) => {
  return preparePetAge(age - 1)
}
