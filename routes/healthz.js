const express = require('express')
const router = express.Router()
const Action = require('../actions/healthz')
const { environmentVariablesToAssert } = require('../infrastructure/environment')
const { addToRequestContext } = require('../infrastructure/RequestContext')
const SUCCESS = 200

router.get('/healthz', async (req, res, next) => {
  try {
    addToRequestContext('controller', {
      ocurredOn: 'healthz',
      action: 'get'
    })
    environmentVariablesToAssert()
    const result = await Action.healthz.invoke()
    const response = addResponseEnvironments(result)
    res.status(SUCCESS).send(response)
  } catch (error) {
    error.data = addResponseEnvironments({})
    next(error)
  }
})

const addResponseEnvironments = (response) => {
  const result = {...response}
  result.environment = {
    NODE_ENV: process.env.NODE_ENV,
    RESTAPI_PORT: process.env.RESTAPI_PORT,
    RESTAPI_VERSION: process.env.RESTAPI_VERSION
  }
  return result
}

module.exports = router
