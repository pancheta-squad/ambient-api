const express = require('express')
const router = express.Router()
const {
  create,
  findById,
  findAll
} = require('../controllers/pets')

router.post('/pets', create)

router.get('/pets/:id', findById)

router.get('/pets', findAll)

module.exports = router
