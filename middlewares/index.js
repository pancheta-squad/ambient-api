const { destroyRequestContext, addToRequestContext } = require('../infrastructure/RequestContext')
const { logger } = require('../infrastructure/logger')
const generateV4UUID = require('../utils/generateUUID')
const ErrorAdapter = require('../infrastructure/errors/ErrorAdapter')

const cors = (req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Methods', '*')
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
  next()
}
const logInfo = (req, res, next) => {
  logger.info(`(${req.method}) => ${req.headers.host}${req.path}`)
  next()
}
const requestTracer = (req, res, next) => {
  let traceRequestId = req.header('traceRequestId')
  if (!traceRequestId) {
    traceRequestId = generateV4UUID()
  }
  addToRequestContext('traceRequestId', traceRequestId)
  res.set('traceRequestId', traceRequestId)
  next()
}
const destroyContext = (req, res, next) => {
  destroyRequestContext()
  next()
}
const error = async (err, req, res, next) => {
  const BAD_REQUEST = 400
  const error = new ErrorAdapter(err)
  logger.error(err.message, error.toLog())
  res.status(BAD_REQUEST).send(error.toResponse())
  next()
}
module.exports = {
  cors,
  logInfo,
  requestTracer,
  error,
  destroyContext
}