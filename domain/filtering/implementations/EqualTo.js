class EqualTo {
  static add (filter) {
    return {
      [filter.field]: {
        '$eq': filter.value
      }
    } 
  }
}

module.exports = EqualTo
