class Pet {
  constructor (name, bornAt) {
    this.name = name
    this.bornAt = bornAt
  }

  addPetInfo (title, description, imagesURL) {
    this.title = title
    this.description = description
    this.imagesURL = imagesURL
    this.errors = []
  }

  isValid () {
    let result = true
    if (!this._hasField('name')) result = false
    if (!this._hasField('bornAt')) result = false
    if (!this._hasField('title')) result = false
    if (!this._hasField('description')) result = false
    if (!this._hasImages()) result = false

    return result
  }

  serialize () {
    return {
      name: this.name,
      bornAt: this.bornAt,
      title: this.title,
      description: this.description,
      imagesURL: this.imagesURL
    }
  }

  _hasField (field) {
    if (!this[field]) this.errors.push(`Field ${field} is required.`)

    return Boolean(this[field])
  }

  _hasImages () {
    let result = true
    if (!Array.isArray(this.imagesURL)) {
      this.errors.push(`Field images needs to be an array.`)
      result = false
    } 
    if (!this.imagesURL.length) {
      this.errors.push(`Field images is required.`)
      result = false
    }

    return result
  }
}

module.exports = Pet