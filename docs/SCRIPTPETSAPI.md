# Script `petsapi.sh`

El script `petsapi.sh` debe lanzarse como un script de Bash, bien con el comando `bash` o con el comando `source`.

Si desea saber los comandos que puede utilizar con el script, ejecute `bash petsapi.sh` sin ningún parámetro para leer la ayuda.
