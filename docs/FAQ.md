# FAQ

## ¿Como puedo tener mi API levantada y lanzar los test si escuchan en el mismo puerto?

Para ello hemos desacoplado la lógica para levantar el server de el `index.js` o punto de entrada de la app.
En el `package.json` el main ahora levanta el servidor. El cual se ejecuta con npm start usando el `/infrastructure/server.js`
Y para lanzar los test `supertest` se apoya en el `index.js`, ejecutando todos los métodos de nuestra express API

## Explicando docker 

`--rm`: Borra el contenedor cuando finaliza su ejecución.

## Explicando '.gitlab-ci.yml'

El archivo `.gitlab-ci.yml` es el encargado de lanzar la IC en gitlab.

Este archivo se divide en varias partes:

a. La información de la imagen, variables, stages, ..., que definen la IC con gitlab.

b. Cada uno de los 'stages' que se utilizan.

En cada stage podemos definir cómo actuará (etiquetas stage, only, except, ...) y qué debe hacer (etiquetas before_script, script, after_script) así como los 'artifacts' que se crean en cada stage para ser consumido por el siguiente stage.

Para poder conectar con el registro de imágenes docker de gitlab necesitamos identificarnos, lo cual se consigue con el siguiente comando:

~~~
echo -n $CI_REGISTRY_PASSWORD | docker login -u $CI_REGISTRY_USER --password-stdin $CI_REGISTRY
~~~

Con el afán de mejorar la interacción entre los distintos desarrolladores, hemos personalizado las imágenes de test de forma que para cada usuario se crean sus propias imágenes, evitando de esta forma que ejecuciones simultáneas sobreescriban imágenes de test de otros lanzamientos. La variable utilizada es 'GITLAB_USER_ID', la cual está indicada en el nombre de la imagen.

Puedes ver la documentación de gitlab para ver otras variables que puedes usar ( `https://docs.gitlab.com/ee/ci/variables/predefined_variables.html#predefined-variables-reference` ).

En los pipeline de la gitlab-ci hay que tener en cuenta que estamos trabajando sobre una virtualización. Dentro del pipeline se levanta una imagen dockerizada que descarga nuestro repositorio, y es aquí donde trabajamos; pero, cuando lanzamos comandos docker en el pipeline estamos construyendo una virtualización docker dentro de la de gitlab-ci. Es importante entender este concepto ya que comúnmente nos lleva a confusión y damos por supuesto que todo es el mismo entorno.

En el caso del pipeline que hemos realizado verás que creamos en tiempo de ejecución el archivo `.env`. Este archivo no existe dentro de la imagen que tenemos dockerizada, por lo que debemos indicar al 'docker run' que nos incluya el archivo `.env` que hemos creado en tiempo de ejecución agregando `--env-file .env`.
