# Docker

En el proyecto ya tenemos creado el archivo 'Dockerfile', este es el utilizado para generar la imagen docker del proyecto.

Teniendo un dockerfile ya preparado, tendremos que preparar una imagen docker del proyecto con:

~~~
docker build -t pets-api .
~~~

O con el script `petsapi.sh`:

~~~
bash petsapi.sh docker:build
~~~

Una vez preparada la imagen, podemos levantar la imagen bien con los datos a través de el archivo `.env`:

~~~
docker run --rm --network host --env-file .env  pets-api npm start
~~~

O con los datos a través de Doppler:

~~~
docker run --rm --network host --env-file <(doppler -c dev_env_local secrets download --no-file --format docker)  pets-api npm start
~~~ 

O con el script `petsapi.sh`:

~~~
bash petsapi.sh docker:start
~~~

Y para parar la imagen podemos lanzar:

~~~
docker stop pets-api
~~~

O con el script `petsapi.sh`:

~~~
bash petsapi.sh docker:stop
~~~
