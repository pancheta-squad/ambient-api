const Pets = require('../../services/pets')

class Find {
  static async invoke (filters) {
    return Pets.service.find(filters)
  }
}

module.exports = Find