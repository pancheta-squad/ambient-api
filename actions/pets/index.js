const FindById = require('./FindById')
const Find = require('./Find')
const FindAll = require('./FindAll')
const Create = require('./Create')

module.exports = {
  create: Create,
  findById: FindById,
  find: Find,
  findAll: FindAll
}