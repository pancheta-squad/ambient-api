const Action = require('../../actions/pets')
const { addToRequestContext } = require('../../infrastructure/RequestContext')
const SUCCESS = 200

const create = async (req, res, next) => {
  try {
    addToRequestContext('controller', {
      ocurredOn: 'pets',
      action: 'create'
    })
    const pet = req.body
    const response = await Action.create.invoke(pet)

    res.status(SUCCESS).send(response)
  } catch (error) {
    next(error)
  }
}

const findById = async (req, res, next) => {
  try {
    addToRequestContext('controller', {
      ocurredOn: 'pets',
      action: 'findById'
    })

    const id = req.params.id
    const response = await Action.findById.invoke(id)

    res.status(SUCCESS).send(response)
  } catch (error) {
    next(error)
  }
}

const findAll = async (req, res, next) => {
  try {
    const filters = req.query.filters || ''
    addToRequestContext('controller', {
      ocurredOn: 'pets',
      action: 'findAll',
      filters
    })

    let response
    if (filters) {
      response = await Action.find.invoke(filters)
    } else {
      response = await Action.findAll.invoke()
    }

    res.status(SUCCESS).send(response)
  } catch (error) {
    next(error)
  }
}

module.exports = {
  create,
  findById,
  findAll
}